Au cas ou vous voudriez tester notre code : 

Rendez vous dans le dossier csv_captcha et utilisez les commandes suivantes : 

Génération des échantillons d'entrainement et entrainement du modéle
$ python start.py 

Génération et reconaissance de captcha aléatoires
$ python test.py

Start.py peut mettre quelques minutes voir 1h à s’exécuter en fonction de votre ordinateur. L’apprentissage est en effet un processus lent et gourmand en ressources


