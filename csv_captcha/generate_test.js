var svgCaptcha = require('svg-captcha');
var fs = require('fs');
path = "csv_test"

//on génére le dossier de stockage des captcha
fs.mkdir(path,function(e){
    if(!e || (e && e.code === 'EEXIST')){
        //do something with contents
    } else {
        //debug
        console.log(e);
    }
});

console.log("/*/ Génération des échantillons de test");

for(i=0;i<10;i++){
  var captcha = svgCaptcha.create({noise : 1});

  fs.writeFile("csv_test/"+ captcha.text + '.svg', captcha.data , function (err) {
    if (err) throw err;
    //console.log('Saved!');
  });
}
