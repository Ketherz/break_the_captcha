

var svgCaptcha = require('svg-captcha');
var fs = require('fs');

path = "csv_sample"

//on génére le dossier de stockage des captcha
fs.mkdir(path,function(e){
    if(!e || (e && e.code === 'EEXIST')){
        //do something with contents
    } else {
        //debug
        console.log(e);
    }
});


//On génére tout les captchas. Pour ce faire, on va boucler la génération des captchas a partir de toutes les lettres et les chiffres
//Puis nous allons stocker le nombre de lettres présentes dans ce string dans un dictionnaire.
//Lorsque le nombre d'occurence d'un caractére atteind 200 ou plus on le retire du preset afin de cesser la génération de cette lettre

console.log("/*/ Génération d'un captchas : ");


//ici on crée un dictionnaire comprennant toutes les lettres dans le dico
done = false;
preset = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
char_count = {};
for(i=0; i <preset.length; i++){
  char_count[preset[i]] = 0;
}

console.log(char_count);

while(done == false) {

  //on génére un captcha au hasard grace au preset
  var captcha = svgCaptcha.create({noise : 1, charPreset : preset});

  //on récupére tout les caractéres utilisé dans le captcha et on incrémentes les valeurs correspondantes dans le dictionnaire
  for(i=0; i <= captcha.text.length ; i++){
    char_count[captcha.text[i]]++;

    //si un caractére posséde plus de 200 génération, on le supprime du préset
    if(char_count[captcha.text[i]] >= 200){
      preset = preset.replace(captcha.text[i],'');
      console.log("Nouvelle chaine : " + preset );
    }

    //si il n'y a plus de caractére dans le preset on change le boolean
    if(preset == ''){
      console.log("/*/ fin de la génération");
      done = true;
    }

  }
  console.log(char_count);


  //On génére le .csv
  fs.writeFile("csv_sample/" + captcha.text + '.svg', captcha.data , function (err) {
    if (err) throw err;
    //console.log('Saved!');
  });




}
