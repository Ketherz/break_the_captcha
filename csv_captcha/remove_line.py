import sys
from PIL import Image
from PIL import ImageFilter
import PIL.ImageOps
from os import walk
import os

def prepare_image(img):
    """Transform image to greyscale and blur it"""
    img = img.filter(ImageFilter.SMOOTH_MORE)
    img = img.filter(ImageFilter.SMOOTH_MORE)
    if 'L' != img.mode:
        img = img.convert('L')
    return img

def remove_noise(img, pass_factor):
    for column in range(img.size[0]):
        for line in range(img.size[1]):
            value = remove_noise_by_pixel(img, column, line, pass_factor)
            img.putpixel((column, line), value)
    return img

def remove_noise_by_pixel(img, column, line, pass_factor):
    if img.getpixel((column, line)) < pass_factor:
        return (0)
    return (255)



def remove_line(path, path_to_save) :

    _, _, filenames = next(walk(path))

    for filename in filenames :


        print("traitement de : " + filename )

        output_image = filename
        pass_factor = 170

        img = Image.open(path + "/" + filename)
        img = prepare_image(img)
        img = remove_noise(img, pass_factor)

        #erosion_img = img.filter(ImageFilter.MinFilter(1))

        img.save(path_to_save + "/" + output_image)
        #erosion_img.save(output_image)
        os.remove(path + "/" + filename)
