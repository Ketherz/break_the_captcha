import matplotlib.pyplot as plt
import numpy as np
import os
from PIL import Image
import tensorflow as tf
import matplotlib.pyplot as plt
import pathlib

from cutter import split_chars_v2
from tensorflow import keras
from tensorflow.keras import layers
from tensorflow.keras.models import Sequential
from os import walk


img_height = 180
img_width = 180
#class_names = []
batch_size = 32
num_classes = 62

"""
path_dataset : chemin vers dataset (ex: cutted/)
"""
def get_dataset(path) :
    #génération du set
    data_dir = pathlib.Path(path)

    train_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        subset="training",
        seed=123,
        image_size=(img_height, img_width),
        batch_size=batch_size)

    val_ds = tf.keras.preprocessing.image_dataset_from_directory(
        data_dir,
        validation_split=0.2,
        subset="validation",
        seed=123,
        image_size=(img_height, img_width),
        batch_size=batch_size)

    return (train_ds, val_ds)

"""
path_dataset : chemin vers dataset (ex: cutted/)
path_to_save : chemin pour sauvegarder le modele de réseau de neurones
"""

def init_network(path_dataset, path_to_save) :
    #importation des images
    data_dir = pathlib.Path(path_dataset)

    #compte :
    image_count = len(list(data_dir.glob('*/*.png')))
    print("Il y a " + str(image_count) + " images d'apprentissage")




    #les sets d'entrainement ?
    """
    train_ds = tf.keras.preprocessing.image_dataset_from_directory(
      data_dir,
      validation_split=0.2,
      subset="training",
      seed=123,
      image_size=(img_height, img_width),
      batch_size=batch_size)

    #les sets de validation?
    val_ds = tf.keras.preprocessing.image_dataset_from_directory(
      data_dir,
      validation_split=0.2,
      subset="validation",
      seed=123,
      image_size=(img_height, img_width),
      batch_size=batch_size)
    """
    #Création du jeu de données :
    train_ds, val_ds = get_dataset(path_dataset)
    #global class_names
    class_names = train_ds.class_names
    print(class_names)

    #Visualiser les données
    """plt.figure(figsize=(10, 10))
    for images, labels in train_ds.take(1):
      for i in range(9):
        ax = plt.subplot(3, 3, i + 1)
        plt.imshow(images[i].numpy().astype("uint8"))
        plt.title(class_names[labels[i]])
        plt.axis("off")
    """

    AUTOTUNE = tf.data.AUTOTUNE

    train_ds = train_ds.cache().shuffle(1000).prefetch(buffer_size=AUTOTUNE)
    val_ds = val_ds.cache().prefetch(buffer_size=AUTOTUNE)

    normalization_layer = layers.experimental.preprocessing.Rescaling(1./255)


    #création du modéle




    model = Sequential([
      layers.experimental.preprocessing.Rescaling(1./255, input_shape=(img_height, img_width, 3)),
      layers.Conv2D(16, 3, padding='same', activation='relu'),
      layers.MaxPooling2D(),
      layers.Conv2D(32, 3, padding='same', activation='relu'),
      layers.MaxPooling2D(),
      layers.Conv2D(64, 3, padding='same', activation='relu'),
      layers.MaxPooling2D(),
      layers.Flatten(),
      layers.Dense(128, activation='relu'),
      layers.Dense(num_classes)
    ])

    #compilation du modéle
    model.compile(optimizer='adam',
                  loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])


    model.summary()


    #Entrainement (5 passes)
    epochs=5
    history = model.fit(
      train_ds,
      validation_data=val_ds,
      epochs=epochs
    )


    """
    #Vue graphique de l'entrainement il y a un risque de suraprentissage, on verra si il faut que je
    #réajuste les étapes

    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']

    loss = history.history['loss']
    val_loss = history.history['val_loss']

    epochs_range = range(epochs)

    plt.figure(figsize=(8, 8))
    plt.subplot(1, 2, 1)
    plt.plot(epochs_range, acc, label='Training Accuracy')
    plt.plot(epochs_range, val_acc, label='Validation Accuracy')
    plt.legend(loc='lower right')
    plt.title('Training and Validation Accuracy')

    plt.subplot(1, 2, 2)
    plt.plot(epochs_range, loss, label='Training Loss')
    plt.plot(epochs_range, val_loss, label='Validation Loss')
    plt.legend(loc='upper right')
    plt.title('Training and Validation Loss')
    plt.show()
    """

    #On va pas mettre les chapitres sur l'augmentation des données ni sur le dropout
    #je ne pense pas que ça soit necessaire car on a beaucoup de valeurs

    model.save(path_to_save)

"""
path_model : chemin où charger le modele réseau de neurones
path_image : chemin de l'image à reconnaitre
path_dataset : chemin des images qui servent à la reconnaissance
"""
def recognition(path_model, path_image, path_dataset) :


    train_ds, val_ds = get_dataset(path_dataset)
    #global class_names
    class_names = train_ds.class_names
    print("class names = ", class_names)

    #On va pas mettre les chapitres sur l'augmentation des données ni sur le dropout
    #je ne pense pas que ça soit necessaire car on a beaucoup de valeurs

    model = tf.keras.models.load_model(
    path_model, custom_objects=None, compile=True, options=None)


    #test sur une autre image

    _, _, filenames = next(walk(path_image))
    letters = []

    for i in filenames :
        img = keras.preprocessing.image.load_img(
            path_image  + i, target_size=(img_height, img_width)
        )
        img_array = keras.preprocessing.image.img_to_array(img)
        img_array = tf.expand_dims(img_array, 0) # Create a batch

        predictions = model.predict(img_array)
        score = tf.nn.softmax(predictions[0])

        """
        print(
            "This image most likely belongs to {} with a {:.2f} percent confidence. Nom image : {}"
            .format(class_names[np.argmax(score)], 100 * np.max(score), i)
        )
        """
    #print(np.argmax(predictions))
        letters.append(class_names[np.argmax(score)])

    return letters

"""
Fonction qui reconnait les captchas entiers (plusieurs caractères), situés dans
le répertoire 'path_captcha', en séparant les caractères du captcha et en
reconnaissant chaque caractère un à un avec le réseau de neurones chargé ('path_model')


path_model : chemin vers le model du réseau de neurones à charger
path_captcha : chemin vers le répertoire contenant les captchas à reconnaitre
path_dataset : path_dataset : chemin des images qui servent à la reconnaissance
path_to_save : chemin du répertoire vers lequel sauvegarder les captchas avec les caractères reconnus en filename
nbchar :nombre de caractères dans les captchas (par défaut 4)
"""
def recognition_whole_captcha(path_model, path_captcha, path_dataset, path_to_save="reconnus/", nbchar=4):
    # récupère le dataset d'entrainement du réseau de neurones
    train_ds, val_ds = get_dataset(path_dataset)
    # class names du réseau
    class_names = train_ds.class_names
    print("class names = ", class_names)
    # on charge le modèle
    model = tf.keras.models.load_model(
    path_model, custom_objects=None, compile=True, options=None)

    _, _, filenames = next(walk(path_captcha))
    path_split_captcha = "cutted_captchas/"
    if (not os.path.isdir(path_split_captcha)):
        os.mkdir(path_split_captcha, mode=744)
    nb_captchas = 0 # nb de captchas total à reconnaitre
    nb_reconnus = 0 # nb de captchas reconnus (en se basant sur le nom du fichier captcha original)
    for f in filenames:
        nb_captchas += 1
        predicted_chars = [] # contient les caractères reconnus
        # séparer les caractères du captcha 'f'
        split_chars_v2(f[:-4], path_captcha, path_split_captcha, nbchar)
        _, _, chars = next(walk(path_split_captcha))
        # on parcourt les caractères séparés par split_chars
        for char in chars:
            img = keras.preprocessing.image.load_img(
                path_split_captcha  + char, target_size=(img_height, img_width)
                )
            img_array = keras.preprocessing.image.img_to_array(img)
            img_array = tf.expand_dims(img_array, 0) # Create a batch

            predictions = model.predict(img_array)
            score = tf.nn.softmax(predictions[0])
            predicted_chars.append(class_names[np.argmax(score)][:1])

            img.close()
            img = None
            # on supprime les fichiers générés dans "cutted/" par split_chars
            os.remove(path_split_captcha + char)

        # sauvegarder captcha avec en nom les caractères reconnus
        # on crée le répertoire de destination s'il n'existe pas
        #path_to_save = "reconnus/"
        if (not os.path.isdir(path_to_save)):
            os.mkdir(path_to_save, mode=744)
        image = Image.open(path_captcha + f)
        #print("image.save("+ path_to_save + "".join(predicted_chars) + ".png" + ") : predicted_chars = "+ str(predicted_chars))
        image.save(path_to_save + "".join(predicted_chars) + ".png")
        image.close()
        print("Captcha à reconnaitre : {} ; caractères reconnus : {}".format(f, predicted_chars))
        # on check s'il a bien reconnu
        vrai_chars = f[:-4]
        #print("vrai chars = " + str(vrai_chars) + " ; predicted_chars = " + "".join(predicted_chars))
        if (vrai_chars == "".join(predicted_chars)):
            nb_reconnus += 1




    print("Il y a {} / {} captchas reconnus !".format(nb_reconnus, nb_captchas))
































#/*end of file*/
