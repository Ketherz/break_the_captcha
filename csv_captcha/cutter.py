from PIL import Image
from os import walk
import os, os.path
import random

def cutter(path, path_to_save):
    _, _, filenames = next(walk(path))
    #call(["node", "sempling.js"])

    for filename in filenames :
        filename = filename[:-4] # enleve l'extension
        split_chars(filename, path+ "/", path_to_save + "/", 4)

"""
fonction qui prend en entrée un captcha clean de X caractères
et qui sépare chaque caractère, découpage en "fenetre" avec largeur
qui vaut largeur_captcha / X. L'image du caractère découpé est save
dans le répertoire du nom du char.

filename : nom du captcha, juste les caractères du captcha dans l'ordre
(pas de path, pas d'extension), exemple : 2A3X
path_load : chemin pour accéder au fichier 'filename' (avec '/')
path_save : chemin où sauvegarder les résultats (avec '/')
nbchar : nb de caractères dans le captcha
extension : l'extension du fichier en entrée (par défaut .png)
padding : nb de pixels à retirer de chaque côté de l'image
(diminue la largeur totale de l'image)(par défaut 10 pixels)

"""
def split_chars(filename, path_load, path_save, nbchar, extension=".png", padding=10):
    # Download Image:
    #nbchar = len(filename)
    #print("nbchar = ", nbchar)
    im = Image.open(path_load+filename+extension)

        # Check Image Size

    im_size = im.size
    #print("size image : ", im_size)

        # Define box inside image
    #padding = 10 # espace vide avant et après le mot
    left = padding
    top = 0
    width = (im_size[0] - padding*2) // nbchar
    height = im_size[1]
    for i in range(nbchar):
        # Create Box
        box = (left, top, left+width, top+height)

        # Crop Image
        # on garde la partie de l'image qui est dans les dimensions
        # de box
        area = im.crop(box)

        # Save Image


        # pour créer des répertoires différents entre majuscule et
        # minuscule sur Windows
        if str(filename[i]).islower():
            DIR = path_save + str(filename[i]) + "_minuscule"
        else:
            DIR = path_save + str(filename[i]) + "_majuscule"

        #DIR = path_save + str(filename[i])
        if (not os.path.isdir( DIR)):
            os.mkdir( DIR, mode=744)
        #print("size area découpée : ", area.size)
        nb = len([name for name in os.listdir(DIR) if os.path.isfile(os.path.join(DIR,name))])
        area.save( DIR +"/"+str(nb+1)+".png", "PNG")
        left += width

    print(filename + " : cutted")

def split_chars_v2(filename, path_load, path_save, nbchar, extension=".png", padding=10):
    # Download Image:
    #nbchar = len(filename)
    #print("nbchar = ", nbchar)
    im = Image.open(path_load+filename+extension)

        # Check Image Size

    im_size = im.size
    #print("size image : ", im_size)

        # Define box inside image
    #padding = 10 # espace vide avant et après le mot
    left = padding
    top = 0
    width = (im_size[0] - padding*2) // nbchar
    height = im_size[1]
    for i in range(nbchar):
        # Create Box
        box = (left, top, left+width, top+height)

        # Crop Image
        # on garde la partie de l'image qui est dans les dimensions
        # de box
        area = im.crop(box)

        # Save Image
        liste_chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
        #area.save( path_save +str("".join(["".join(random.choices(liste_chars)) for i in range(8)]))+".png", "PNG")
        area.save(path_save + str(i)+".png")
        left += width
