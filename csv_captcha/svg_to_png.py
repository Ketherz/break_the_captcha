from os import walk
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPDF, renderPM
from subprocess import call
import os


def generate_and_convert(path, path_to_save) :
    call(["node", "sempling.js"])

    _, _, filenames = next(walk(path))

    for i in filenames :
        print("convertion de : " + i[:4])
        drawing = svg2rlg(path + "/" + i)
        renderPM.drawToFile(drawing, path_to_save + "/"+ i[:4] +".png", fmt="PNG")
        os.remove(path + "/" + i)
