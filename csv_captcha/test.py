from neuronal_network import recognition
import pathlib
import os
from os import walk
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPDF, renderPM
from subprocess import call
from remove_line import remove_line
from cutter import cutter
from neuronal_network import init_network, recognition_whole_captcha

call(["node", "generate_test.js"])

#liste et converti les captcha à tester
_, _, filenames = next(walk("csv_test"))

for i in filenames :
    print("convertion de : " + i[:4])
    drawing = svg2rlg("csv_test/" + i)
    renderPM.drawToFile(drawing, "csv_test/"+ i[:4] +".png", fmt="PNG")
    os.remove("csv_test/" + i)


#On effectue le post traitement sur les images
remove_line("csv_test","pretreatment_test")

# répertoire contenant les images à reconnaitre
# dir_images = pathlib.Path("cutted_test/0_majuscule")

# liste des images PNG
#captcha = recognition("neuronal_network", "cutted_test/0_majuscule/" , "cutted")
recognition_whole_captcha("neuronal_network","pretreatment_test/" , "cutted/")
