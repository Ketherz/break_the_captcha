from svg_to_png import generate_and_convert
from remove_line import remove_line
from cutter import cutter
from neuronal_network import init_network


generate_and_convert("csv_sample", "csv_converted_sample")

remove_line("csv_converted_sample","pretreatment")

cutter("pretreatment", "cutted")

init_network("cutted", "neuronal_network")
