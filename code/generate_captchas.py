import random
import cv2
import string
import numpy as np
import os
from PIL import ImageFont, ImageDraw, Image, ImageChops

def un_char(path_save):
    size = 40
    height = size*2
    padding = 10
    width = size + padding
    font = ImageFont.truetype("arial.ttf", size=size)
    img = np.zeros((height, width,3), np.uint8)
    img_pil = Image.fromarray(img+255)
    draw = ImageDraw.Draw(img_pil)
    text = ''.join(random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase)) # 1 char
    draw.text((10,10), text, font=font, align="ms", 
        fill=(random.randint(0,255), random.randint(0,255), random.randint(0,255)))
    img = np.array(img_pil)
    cv2.imwrite(f"{path_save}/{text}.png", img) #if you want to save the image
    return text

def generate(nb, path_save):
    size = 40
    filename = []
    images_chars = []
    for i in range(nb):
        char = un_char(path_save)
        filename.append(char)
        path = path_save + "/" + str(char)+ ".png"
        images_chars.append((Image.open(path), path))

    print("filename : ", "".join(filename), " ; images = ", images_chars)

    im_size = images_chars[0][0].size # hauteur, largeur d'une image d'un char 

    img = np.zeros((im_size[1], im_size[0]*nb, 3), np.uint8)
    img_pil = Image.fromarray(img+255)

    left = 0
    top = 0
    width = im_size[0]
    height = im_size[1]
    box = (left, top, left+width, top+height)

    for img, path in images_chars:
    #draw = ImageDraw.Draw(img_pil)
        img_pil.paste(img, box=box) # copie du char suivant dans l'image du futur captcha
        left += width
        box = (left, top, left+width, top+height) # zone dans laquelle sera copié le prochain char
        img.close()
        os.remove(path)
    img_pil.save(path_save + "/" + "".join(filename) + ".png") # save de l'image du captcha

    

    
    """
    # Setting up the canvas
    #size = random.randint(10,16)
    size = 40
    #length = random.randint(4,8)
    length = 4
    for _ in range(nb):
        #img = np.zeros(((size*2)+5, length*size, 3), np.uint8)
        img = np.zeros((80, 120, 3), np.uint8)
        img_pil = Image.fromarray(img+255)

        # Drawing text and lines
        #font = ImageFont.truetype(random.choice(fonts), size)
        font = ImageFont.truetype("arial.ttf", size=size)
        draw = ImageDraw.Draw(img_pil)
        text = ''.join(
            random.choice(string.ascii_uppercase + string.digits + string.ascii_lowercase) 
               for _ in range(length))
        draw.text((10, 10), text, font=font, align="ms", 
              fill=(random.randint(0,255), random.randint(0,255), random.randint(0,255)))
        
        draw.line([(random.choice(range(length*size)), random.choice(range((size*2)+5)))
           ,(random.choice(range(length*size)), random.choice(range((size*2)+5)))]
          , width=1, fill=(random.randint(0,255), random.randint(0,255), random.randint(0,255)))
        
        # Adding noise and blur
        img = np.array(img_pil)
    
        thresh = random.randint(1,5)/100
        for i in range(img.shape[0]):
            for j in range(img.shape[1]):
        rdn = random.random()
        if rdn < thresh:
            img[i][j] = random.randint(0,123)
        elif rdn > 1-thresh:
            img[i][j] = random.randint(123,255)
       #img = cv2.blur(img,(int(size/random.randint(5,10)),int(size/random.randint(5,10))))

        #Displaying image
        cv2.imshow(f"{text}", img)
        cv2.waitKey()
        cv2.destroyAllWindows()
        
        #OUTPUT_FOLDER = "pretreatment_test"
        OUTPUT_FOLDER = path_save
        cv2.imwrite(f"{OUTPUT_FOLDER}/{text}.png", img) #if you want to save the image
    """

generate(4, "test")
