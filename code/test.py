from neuronal_network import recognition
import pathlib
import os
from os import walk
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPDF, renderPM
from subprocess import call
from remove_line import remove_line
from cutter import cutter
from neuronal_network import init_network, recognition_whole_captcha

from generate_captchas import generate
"""
fonction qui va générer un certain nombre 'nb' de captchas et les
sauvegarde dans le répertoire 'path_to_save'
nb (int) : nombre de captcha
path_to_save (string) : chemin relatif à partir de l'exécution du fichier
"""
def generation_captchas(nb, path_to_save):
    generate(nb, path_to_save)
    print("Génération de captchas terminée !")

"""
fonction qui effectue le pré-traitement sur les captchas 
(remove line, enlève couleur du background ...)
"""
def pretraitement(path_load, path_save):
    pass

call(["node", "generate_test.js"])
#generation_captchas(2, "pretreatment_test")

#liste et converti les captcha à tester
_, _, filenames = next(walk("csv_test"))

for i in filenames :
    print("convertion de : " + i[:4])
    drawing = svg2rlg("csv_test/" + i)
    renderPM.drawToFile(drawing, "csv_test/"+ i[:4] +".png", fmt="PNG")
    os.remove("csv_test/" + i)


#On effectue le post traitement sur les images
remove_line("csv_test","pretreatment_test")

# répertoire contenant les images à reconnaitre
# dir_images = pathlib.Path("cutted_test/0_majuscule")

# liste des images PNG
#captcha = recognition("neuronal_network", "cutted_test/0_majuscule/" , "cutted")
recognition_whole_captcha("neuronal_network","pretreatment_test/" , "cutted/")
